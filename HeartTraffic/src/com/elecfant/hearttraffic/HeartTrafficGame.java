package com.elecfant.hearttraffic;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.elecfant.hearttraffic.entities.Grid;

public class HeartTrafficGame implements ApplicationListener {
	private static final float BEAT_RATE = 1.2f; // heartbeat each BEAT_RATE
	private static final short GAME_MENU = 0;
	private static final short GAME_PLAY = 1;
	private static final short GAME_WIN = 2;
	private static final short GAME_LOST = 3;
	private static final short GAME_PAUSE = 4;
	private static final short GAME_HELP = 5;
	private static final short GAME_LEVEL = 6;
	private SpriteBatch batch;
	private int w;
	private int h;
	private Grid mainGrid;
	private Texture levelTexture;
	private Texture backgroundTexture;

	private BitmapFont titleFont;

	private Texture cellTexture;

	private Sound heartbeat;

	private float since_heartbeat = BEAT_RATE;

	private OrthographicCamera camera;
	private Vector3 touchPoint;

	private short gameState = GAME_MENU;
	private FileHandle[] levelFiles;
	private FileHandle currentLvl;
	private FileHandle[] editorLevels;

	@Override
	public void create() {
		titleFont = new BitmapFont(Gdx.files.internal("fonts/default_large.fnt"), false);
		switch (Gdx.app.getType()) {
		case Android:
			editorLevels = Gdx.files.external("Games/Heart Traffic/levels").list();
			break;
		case Desktop:
			editorLevels = Gdx.files.external("%APPDATA%/Heart Traffic/levels").list();
			break;
		default:
			break;
		}
		if (Gdx.app.getType() != ApplicationType.Desktop) {
			levelFiles = Gdx.files.internal("levels").list();
		} else {
			levelFiles = new FileHandle[] {
					Gdx.files.internal("levels/Lvl0.hlv"),
					Gdx.files.internal("levels/Lvl1.hlv"),
					Gdx.files.internal("levels/Lvl2.hlv"),
					Gdx.files.internal("levels/Lvl3.hlv"),
					Gdx.files.internal("levels/Lvl4.hlv"),
					Gdx.files.internal("levels/Lvl5.hlv")
			};
		}
		if (editorLevels != null && editorLevels.length > 0) {
			FileHandle[] temp = new FileHandle[levelFiles.length + editorLevels.length];
			System.arraycopy(levelFiles, 0, temp, 0, levelFiles.length);
			System.arraycopy(editorLevels, 0, temp, levelFiles.length, editorLevels.length);
			levelFiles = temp;
		}
		Gdx.input.setInputProcessor(new InputProcessor() {

			@Override
			public boolean touchUp(int screenX, int screenY, int pointer, int button) {
				touchPoint.set(screenX, screenY, 0);
				camera.unproject(touchPoint);
				switch (gameState) {
				case GAME_PLAY:
					mainGrid.touchUp((int) (touchPoint.x), (int) (touchPoint.y), pointer, button);
					break;
				case GAME_PAUSE:
					TextBounds ng = titleFont.getBounds("Continue");
					Rectangle ng_rect = new Rectangle((w - ng.width) / 2, 250 - ng.height, ng.width, ng.height);
					if (ng_rect.contains(touchPoint.x, touchPoint.y)) {
						gameState = GAME_PLAY;
					}

					TextBounds m = titleFont.getBounds("Menu");
					Rectangle m_rect = new Rectangle((w - m.width) / 2, 150 - m.height, m.width, m.height);
					if (m_rect.contains(touchPoint.x, touchPoint.y)) {
						gameState = GAME_MENU;
					}

					TextBounds q = titleFont.getBounds("Quit");
					Rectangle q_rect = new Rectangle((w - q.width) / 2, 50 - q.height, q.width, q.height);
					if (q_rect.contains(touchPoint.x, touchPoint.y)) {
						Gdx.app.exit();
					}
					break;
				case GAME_WIN:
				case GAME_HELP:
				case GAME_LOST:
					String back = " Back to menu";
					TextBounds tb = titleFont.getBounds(back);
					Rectangle back_rect = new Rectangle(0, 10, tb.width, tb.height);
					if (back_rect.contains(touchPoint.x, touchPoint.y)) {
						gameState = GAME_MENU;
					}
					break;
				case GAME_LEVEL:

					for (int i = 0; i < levelFiles.length; ++i) {
						String name = levelFiles[i].nameWithoutExtension();
						tb = titleFont.getBounds(name);
						Rectangle level_rect = new Rectangle((w - titleFont.getBounds(name).width) / 2, h - (i + 1) * tb.height, tb.width, tb.height);
						if (level_rect.contains(touchPoint.x, touchPoint.y)) {
							currentLvl = levelFiles[i];
							newGame();
							gameState = GAME_PLAY;
							break;
						}
					}
					back = " Back to menu";
					tb = titleFont.getBounds(back);
					back_rect = new Rectangle(0, 10, tb.width, tb.height);
					if (back_rect.contains(touchPoint.x, touchPoint.y)) {
						gameState = GAME_MENU;
					}
					break;
				case GAME_MENU:
					ng = titleFont.getBounds("New Game");
					ng_rect = new Rectangle((w - ng.width) / 2, 250 - ng.height, ng.width, ng.height);
					if (ng_rect.contains(touchPoint.x, touchPoint.y)) {
						gameState = GAME_LEVEL;
					}

					TextBounds ha = titleFont.getBounds("Help and About");
					Rectangle ha_rect = new Rectangle((w - ha.width) / 2, 150 - ha.height, ha.width, ha.height);
					if (ha_rect.contains(touchPoint.x, touchPoint.y)) {
						gameState = GAME_HELP;
					}

					q = titleFont.getBounds("Quit");
					q_rect = new Rectangle((w - q.width) / 2, 50 - q.height, q.width, q.height);
					if (q_rect.contains(touchPoint.x, touchPoint.y)) {
						Gdx.app.exit();
					}
					break;
				}

				return false;
			}

			@Override
			public boolean touchDragged(int screenX, int screenY, int pointer) {
				touchPoint.set(screenX, screenY, 0);
				camera.unproject(touchPoint);
				switch (gameState) {
				case GAME_PLAY:
					mainGrid.touchDragged((int) (touchPoint.x), (int) (touchPoint.y), pointer);
					break;
				case GAME_PAUSE:
					break;
				case GAME_WIN:
					break;
				case GAME_LOST:
					break;
				case GAME_HELP:
					break;
				case GAME_MENU:
					break;
				}

				return false;
			}

			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {
				touchPoint.set(screenX, screenY, 0);
				camera.unproject(touchPoint);
				switch (gameState) {
				case GAME_PLAY:
					mainGrid.touchDown((int) (touchPoint.x), (int) (touchPoint.y), pointer, button);
					break;
				case GAME_PAUSE:
					break;
				case GAME_WIN:
					break;
				case GAME_LOST:
					break;
				case GAME_HELP:
					break;
				case GAME_MENU:
					break;
				}

				return false;
			}

			@Override
			public boolean scrolled(int amount) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean mouseMoved(int screenX, int screenY) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean keyUp(int keycode) {
				switch (gameState) {
				case GAME_PLAY:
					if (Keys.MENU == keycode || Keys.ESCAPE == keycode) {
						gameState = GAME_PAUSE;
					}
					break;
				case GAME_PAUSE:
					break;
				case GAME_WIN:
					break;
				case GAME_LOST:
					break;
				case GAME_HELP:
					break;
				case GAME_MENU:
					break;
				}
				return false;
			}

			@Override
			public boolean keyTyped(char character) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean keyDown(int keycode) {
				// TODO Auto-generated method stub
				return false;
			}
		});
		heartbeat = Gdx.audio.newSound(Gdx.files.internal("sfx/heartbeat.wav"));
		touchPoint = new Vector3(-1, -1, -1);
		w = Gdx.graphics.getWidth();
		h = Gdx.graphics.getHeight();
		Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1);
		camera = new OrthographicCamera();
		camera.setToOrtho(false, w, h);

		FileHandle texFile = Gdx.files.internal("textures/background.jpg");
		backgroundTexture = new Texture(texFile, true);
		backgroundTexture.setFilter(TextureFilter.MipMapLinearLinear, TextureFilter.Linear);

		cellTexture = new Texture(Gdx.files.internal("textures/cell.png"), true);
		cellTexture.setFilter(TextureFilter.MipMapLinearLinear, TextureFilter.Linear);
		batch = new SpriteBatch();
	}

	private void newGame() {
		FileHandle gf = currentLvl;
		FileHandle texFile = Gdx.files.internal("textures/vessels.png");
		String gridData = gf.readString();
		levelTexture = new Texture(texFile, true);
		levelTexture.setFilter(TextureFilter.MipMapLinearLinear, TextureFilter.Linear);
		mainGrid = new Grid(gridData, levelTexture);
		mainGrid.setPosition((w - mainGrid.getW()) / 2, (h - mainGrid.getH()) / 2);
		mainGrid.randomize();
		mainGrid.setCell(cellTexture);

	}

	@Override
	public void dispose() {
		batch.dispose();
		if (levelTexture != null) {
			levelTexture.dispose();
		}
		if (mainGrid != null) {
			mainGrid.dispose();
		}
		cellTexture.dispose();
		heartbeat.stop();
		titleFont.dispose();
		heartbeat.dispose();
		backgroundTexture.dispose();
	}

	@Override
	public void render() {
		update(Gdx.graphics.getDeltaTime());

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		batch.draw(backgroundTexture, 0, 0, w, h);
		switch (gameState) {
		case GAME_PLAY:
			mainGrid.render(batch);
			break;
		case GAME_PAUSE:
			mainGrid.render(batch);
			titleFont.draw(batch, " Heart Traffic", 0, Gdx.graphics.getHeight() - titleFont.getBounds("Heart Traffic").height);
			titleFont.draw(batch, "Continue", (w - titleFont.getBounds("Continue").width) / 2, 250);
			titleFont.draw(batch, "Menu", (w - titleFont.getBounds("Menu").width) / 2, 150);
			titleFont.draw(batch, "Quit", (w - titleFont.getBounds("Quit").width) / 2, 50);
			break;
		case GAME_WIN:
			String msg = "Congratulations! It lives!";
			TextBounds tb = titleFont.getBounds(msg);
			titleFont.draw(batch, msg, (w - tb.width) / 2, (h + tb.height) / 2);
			String back = " Back to menu";
			tb = titleFont.getBounds(back);
			titleFont.draw(batch, back, 0, tb.height + 10);
			break;
		case GAME_LOST:
			msg = "Too bad, it's dead now...";
			tb = titleFont.getBounds(msg);
			titleFont.draw(batch, msg, (w - tb.width) / 2, (h + tb.height) / 2);
			back = " Back to menu";
			tb = titleFont.getBounds(back);
			titleFont.draw(batch, back, 0, tb.height + 10);
			break;
		case GAME_LEVEL:
			int level_count = levelFiles.length;
			back = " Back to menu";
			tb = titleFont.getBounds(back);
			titleFont.draw(batch, back, 0, tb.height + 10);
			for (int i = 0; i < level_count; ++i) {
				String name = levelFiles[i].nameWithoutExtension();
				tb = titleFont.getBounds(name);
				titleFont.draw(batch, name, (w - titleFont.getBounds(name).width) / 2, h - i * tb.height);
			}
			break;
		case GAME_HELP:
			msg = "Connect blood vesells so that blood cells from heart comes back to a heart.\n" +
					"Loose too much blood cells and it is dead.\n" +
					"Keep it alive for specified time and you win.\n" +
					"\n" +
					"\n" +
					"\n" +
					"Programming and etc: Petras Sukys\n" +
					"Alpha tests + levels: Ignas Gerulaitis";
			BitmapFont bfh = new BitmapFont(Gdx.files.internal("fonts/default.fnt"), false);
			bfh.drawWrapped(batch, msg, 10, h - 10, w);
			back = " Back to menu";
			tb = titleFont.getBounds(back);
			titleFont.draw(batch, back, 0, tb.height + 10);
			break;
		case GAME_MENU:

			titleFont.draw(batch, " Heart Traffic", 0, Gdx.graphics.getHeight() - titleFont.getBounds("Heart Traffic").height);
			titleFont.draw(batch, "New Game", (w - titleFont.getBounds("New Game").width) / 2, 250);
			titleFont.draw(batch, "Help and About", (w - titleFont.getBounds("Help and About").width) / 2, 150);
			titleFont.draw(batch, "Quit", (w - titleFont.getBounds("Quit").width) / 2, 50);
			break;
		}
		batch.end();
	}

	private void update(float timeElapsed) {
		switch (gameState) {
		case GAME_PLAY:
			since_heartbeat += timeElapsed;
			mainGrid.update(timeElapsed);
			final byte gridstate = mainGrid.getGameState();
			if (gridstate == Grid.STATE_WIN) {
				gameState = GAME_WIN;
				System.out.println("WIN");
			} else if (gridstate == Grid.STATE_LOST) {
				gameState = GAME_LOST;
				System.out.println("LOST");
			} else {
				if (since_heartbeat >= BEAT_RATE) {
					since_heartbeat -= BEAT_RATE;
					heartbeat.play(1.0f);
					mainGrid.newBloodCell();
				}
			}
			break;
		case GAME_PAUSE:
			break;
		case GAME_WIN:
			break;
		case GAME_LOST:
			break;
		case GAME_HELP:
			break;
		case GAME_MENU:
			break;
		}

	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
