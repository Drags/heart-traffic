package com.elecfant.hearttraffic.entities;

import java.util.Scanner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IdentityMap;

public class Grid extends Entity implements InputProcessor {
	private Array<GridCell> contents;
	private IdentityMap<Short, Short> organs;
	private short grid_w;
	private short grid_h;
	private int cell_width;
	private int cell_height;

	private Texture heartTexture;
	private Texture redArrowTexture;
	private Texture blueArrowTexture;
	private TextureRegion heartTextureRegion;
	private TextureRegion redArrowTextureRegion;
	private TextureRegion blueArrowTextureRegion;

	public final static short ORGAN_HEART_OUT = 0;
	public final static short ORGAN_HEART_IN = 1;

	private final static int tex_cell_width = 120;
	private final static int tex_cell_height = 120;
	public static final byte STATE_LOST = 1;
	public static final byte STATE_PLAY = 0;
	public static final byte STATE_WIN = 2;

	private IdentityMap<Short, TextureRegion> borders;

	private Texture texture;
	private int selected_cell = -1;

	private Texture cellTexture;
	private TextureRegion cellTextureRegion;
	private short max_lost_cells = 1;
	private short cells_lost = 0;
	private int target_time = 0;
	private float time_in_level = 0;
	private byte game_state = 0;
	private BitmapFont default_font = new BitmapFont(Gdx.files.internal("fonts/default.fnt"), false);

	public Grid(String data, Texture texture) {
		heartTexture = new Texture(Gdx.files.internal("textures/heart.png"));
		heartTextureRegion = new TextureRegion(heartTexture);
		redArrowTexture = new Texture(Gdx.files.internal("textures/red.png"));
		blueArrowTexture = new Texture(Gdx.files.internal("textures/blue.png"));
		redArrowTextureRegion = new TextureRegion(redArrowTexture);
		blueArrowTextureRegion = new TextureRegion(blueArrowTexture);
		this.texture = texture;
		Scanner sc = new Scanner(data);
		borders = new IdentityMap<Short, TextureRegion>(4);
		borders.put(GridCell.LEFT, new TextureRegion(
				texture,
				1 * Grid.tex_cell_width,
				3 * Grid.tex_cell_height,
				Grid.tex_cell_width,
				Grid.tex_cell_height
				));
		borders.put(GridCell.RIGHT, new TextureRegion(
				texture,
				2 * Grid.tex_cell_width,
				3 * Grid.tex_cell_height,
				Grid.tex_cell_width,
				Grid.tex_cell_height
				));
		borders.put(GridCell.DOWN, new TextureRegion(
				texture,
				0 * Grid.tex_cell_width,
				3 * Grid.tex_cell_height,
				Grid.tex_cell_width,
				Grid.tex_cell_height
				));
		borders.put(GridCell.UP, new TextureRegion(
				texture,
				3 * Grid.tex_cell_width,
				3 * Grid.tex_cell_height,
				Grid.tex_cell_width,
				Grid.tex_cell_height
				));
		this.max_lost_cells = sc.nextShort();
		this.target_time = sc.nextInt();
		this.grid_w = (short) sc.nextShort();
		this.grid_h = (short) sc.nextShort();
		final int organ_count = sc.nextInt();
		this.cell_height = 360 / grid_h;
		this.cell_width = 360 / grid_w;
		contents = new Array<GridCell>(true, this.grid_w * this.grid_h);
		short in = -1;
		organs = new IdentityMap<Short, Short>(organ_count);
		short type;
		short location;
		for (int i = 0; i < organ_count; ++i) {
			type = sc.nextShort();
			location = sc.nextShort();
			organs.put(location, type);
		}
		for (int j = 0; j < this.grid_h; ++j) {
			for (int i = 0; i < this.grid_w; ++i) {
				type = sc.nextShort();
				in = sc.nextShort();
				GridCell gc = new GridCell(type, in, this);
				gc.x = i;
				gc.y = j;
				switch (in) {
				case 3:
					gc.textureRegion = new TextureRegion(this.texture, 1 * Grid.tex_cell_width, 1 * Grid.tex_cell_height, Grid.tex_cell_width, Grid.tex_cell_height);
					break;
				case 5:
					gc.textureRegion = new TextureRegion(this.texture, 1 * Grid.tex_cell_width, 0 * Grid.tex_cell_height, Grid.tex_cell_width, Grid.tex_cell_height);
					break;
				case 6:
					gc.textureRegion = new TextureRegion(this.texture, 2 * Grid.tex_cell_width, 1 * Grid.tex_cell_height, Grid.tex_cell_width, Grid.tex_cell_height);
					break;
				case 7:
					gc.textureRegion = new TextureRegion(this.texture, 3 * Grid.tex_cell_width, 2 * Grid.tex_cell_height, Grid.tex_cell_width, Grid.tex_cell_height);
					break;
				case 9:
					gc.textureRegion = new TextureRegion(this.texture, 1 * Grid.tex_cell_width, 2 * Grid.tex_cell_height, Grid.tex_cell_width, Grid.tex_cell_height);
					break;
				case 10:
					gc.textureRegion = new TextureRegion(this.texture, 0 * Grid.tex_cell_width, 1 * Grid.tex_cell_height, Grid.tex_cell_width, Grid.tex_cell_height);
					break;
				case 11:
					gc.textureRegion = new TextureRegion(this.texture, 0 * Grid.tex_cell_width, 2 * Grid.tex_cell_height, Grid.tex_cell_width, Grid.tex_cell_height);
					break;
				case 12:
					gc.textureRegion = new TextureRegion(this.texture, 2 * Grid.tex_cell_width, 2 * Grid.tex_cell_height, Grid.tex_cell_width, Grid.tex_cell_height);
					break;
				case 13:
					gc.textureRegion = new TextureRegion(this.texture, 4 * Grid.tex_cell_width, 0 * Grid.tex_cell_height, Grid.tex_cell_width, Grid.tex_cell_height);
					break;
				case 14:
					gc.textureRegion = new TextureRegion(this.texture, 4 * Grid.tex_cell_width, 2 * Grid.tex_cell_height, Grid.tex_cell_width, Grid.tex_cell_height);
					break;
				default:
					gc.textureRegion = new TextureRegion(this.texture, 0 * Grid.tex_cell_width, 0 * Grid.tex_cell_height, Grid.tex_cell_width, Grid.tex_cell_height);
					break;
				}
				contents.add(gc);
			}
		}
		sc.close();
	}

	public void randomize() {
		for (GridCell cell : contents) {
			cell.rotation = (int) (1 + Math.random() * 2) * 90;
		}
	}

	public void render(SpriteBatch batch) {
		for (int i = 0; i < contents.size; ++i) {
			if (i != this.selected_cell) {
				final GridCell cell = contents.get(i);
				batch.draw(
						cell.textureRegion,
						cell.getX() * this.cell_width + this.x,
						cell.getY() * this.cell_height + this.y,
						this.cell_width / 2,
						this.cell_height / 2,
						this.cell_width,
						this.cell_height,
						1,
						1,
						cell.rotation);
			}
		}

		short border;
		Vector2 position = new Vector2();
		for (int i = 0; i < 2 * (this.grid_h + this.grid_w); ++i) {
			border = getBorderFromPerimeterPosition(i);
			position.set(getVectorFromPerimeterPosition(i));
			if (organs.get((short) i) != null) {
				final float rotation;
				switch (border) {
				case GridCell.LEFT:
					rotation = 0;
					break;
				case GridCell.DOWN:
					rotation = 90;
					break;
				case GridCell.RIGHT:
					rotation = 180;
					break;
				case GridCell.UP:
					rotation = 270;
					break;
				default:
					rotation = 0;
				}
				switch (organs.get((short) i)) {
				case ORGAN_HEART_IN:
					batch.draw(heartTextureRegion, position.x, position.y);
					batch.draw(
							blueArrowTextureRegion,
							position.x,
							position.y,
							cell_width / 2,
							cell_height / 2,
							cell_width,
							cell_height,
							0.5f,
							0.5f,
							rotation - 180
							);
					break;
				case ORGAN_HEART_OUT:
					batch.draw(heartTextureRegion, position.x, position.y);
					batch.draw(
							redArrowTextureRegion,
							position.x,
							position.y,
							cell_width / 2,
							cell_height / 2,
							cell_width,
							cell_height,
							0.5f,
							0.5f,
							rotation
							);
					break;
				default:
					batch.draw(borders.get(border), position.x, position.y, cell_width, cell_height);
				}
			} else {
				batch.draw(borders.get(border), position.x, position.y, cell_width, cell_height);
			}
		}

		for (int i = 0; i < contents.size; ++i) {
			if (i != this.selected_cell) {
				final GridCell cell = contents.get(i);
				for (int j = 0; j < cell.bloodCells.size; ++j) {
					Vector2 pos = cell.getCellPosition(j, this.cell_width, this.cell_height);
					float rot = cell.getCellRotation(j);
					batch.draw(
							cellTextureRegion,
							pos.x + this.x + cell.x * this.cell_width,
							pos.y + this.y + cell.y * this.cell_height,
							cellTexture.getWidth() * 0.125f,
							cellTexture.getHeight() * 0.125f,
							cellTexture.getWidth() * 0.25f,
							cellTexture.getHeight() * 0.25f,
							1,
							1,
							rot
							);
				}
			}
		}

		if (this.selected_cell >= 0 && this.selected_cell < contents.size) {
			final GridCell cell = contents.get(this.selected_cell);
			batch.draw(
					cell.textureRegion,
					cell.getX() * this.cell_width + this.x,
					cell.getY() * this.cell_height + this.y,
					this.cell_width / 2,
					this.cell_height / 2,
					this.cell_width,
					this.cell_height,
					1,
					1,
					cell.rotation);
		}
		default_font.setColor(new Color(1f, 1f, 1f, 0.8f));
		final String time = " Time: " + (int) Math.round(time_in_level) + " / " + target_time;
		final String cells = "Cells: " + (max_lost_cells - cells_lost) + " / " + max_lost_cells + " ";
		TextBounds tb = default_font.getBounds(cells);
		default_font.draw(batch, time, 0, tb.height + 10);
		default_font.draw(batch, cells, Gdx.graphics.getWidth() - default_font.getBounds(cells).width, tb.height + 10);
	}

	private short getBorderFromPerimeterPosition(int perimeterPosition) {
		if (perimeterPosition >= this.grid_w + this.grid_h * 2) {
			return GridCell.UP;
		} else if (perimeterPosition >= this.grid_w + this.grid_h) {
			return GridCell.RIGHT;
		} else if (perimeterPosition >= this.grid_h) {
			return GridCell.DOWN;
		} else {
			return GridCell.LEFT;
		}
	}

	private Vector2 getVectorFromPerimeterPosition(int perimeterPosition) {
		short border = getBorderFromPerimeterPosition(perimeterPosition);
		short local_id;
		Vector2 result = new Vector2();
		switch (border) {
		case GridCell.UP:
			local_id = (short) (perimeterPosition - 2 * grid_h - grid_w);
			result.set(
					this.x + local_id * this.cell_width,
					this.y + this.cell_height * this.grid_h
					);
			break;
		case GridCell.DOWN:
			local_id = (short) (perimeterPosition - grid_h);
			result.set(
					this.x + local_id * this.cell_width,
					this.y - this.cell_width
					);
			break;
		case GridCell.RIGHT:
			local_id = (short) (perimeterPosition - grid_h - grid_w);
			result.set(
					this.x + this.grid_w * this.cell_width,
					this.y + local_id * this.cell_height
					);
			break;
		case GridCell.LEFT:
			local_id = (short) perimeterPosition;
			result.set(
					this.x - this.cell_width,
					this.y + local_id * this.cell_height
					);
			break;
		}

		return result;

	}

	public void update(float timeElapsed) {
		time_in_level += timeElapsed;
		for (GridCell cell : contents) {
			cell.update(timeElapsed);
		}

		if (time_in_level >= target_time) {
			this.game_state = STATE_WIN;
		} else if (cells_lost >= max_lost_cells) {
			this.game_state = STATE_LOST;
		}
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		for (int i = 0; i < contents.size; ++i) {
			GridCell cell = contents.get(i);
			Rectangle aabb = new Rectangle(this.x + cell.getX() * cell_width, this.y + cell.getY() * cell_height, this.cell_width, this.cell_height);
			if (aabb.contains(screenX, screenY)) {
				this.selected_cell = i;
				this.cells_lost += this.contents.get(selected_cell).killAllCells();
				break;
			}
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (this.selected_cell >= 0 && this.selected_cell < this.contents.size) {
			contents.get(selected_cell).rotation = (float) (90 * Math.round(contents.get(selected_cell).rotation / 90));
			this.cells_lost += this.contents.get(selected_cell).killAllCells();
			this.selected_cell = -1;
		}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {

		if (selected_cell >= 0 && selected_cell < contents.size) {
			final GridCell cell = contents.get(selected_cell);
			final int rel_X = screenX - this.x - cell.getX() * cell_width - this.cell_width / 2;
			final int rel_Y = screenY - this.y - cell.getY() * cell_height - this.cell_height / 2;
			final float temp_rotation = (float) (Math.atan2(rel_Y, rel_X) * (180 / Math.PI));
			contents.get(selected_cell).rotation = temp_rotation;
			this.cells_lost += this.contents.get(selected_cell).killAllCells();
		}

		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	public int getH() {
		return this.cell_height * grid_h;
	}

	public int getW() {
		return this.cell_width * grid_w;
	}

	public void newBloodCell() {
		if (cellTexture != null) {
			for (short i = 0; i < (grid_w + grid_h) * 2; ++i) {
				if (organs.containsKey(i)) {
					final short type = organs.get(i).shortValue();
					if (type == ORGAN_HEART_OUT) {
						short border = getBorderFromPerimeterPosition(i);
						Vector2 targetGridCell = getGridCellFromPerimeterPosition(i);
						for (int j = 0; j < contents.size; ++j) {
							final GridCell cell = contents.get(j);
							if (cell.x == targetGridCell.x && cell.y == targetGridCell.y) {
								if (!cell.inputBloodCell(border)) {
									++cells_lost;
								}
							}
						}
					}
				}
			}
		}

	}

	private Vector2 getGridCellFromPerimeterPosition(short i) {
		final Vector2 result = new Vector2();
		final short border = getBorderFromPerimeterPosition(i);
		switch (border) {
		case GridCell.DOWN:
			result.set(i - grid_h, 0);
			break;
		case GridCell.UP:
			result.set(i - grid_h * 2 - grid_w, grid_h - 1);
			break;
		case GridCell.RIGHT:
			result.set(grid_w - 1, i - grid_w - grid_h);
			break;
		case GridCell.LEFT:
			result.set(0, i);
			break;
		}
		return result;
	}

	public void setCell(Texture cellTexture) {
		this.cellTexture = cellTexture;
		this.cellTextureRegion = new TextureRegion(cellTexture);

	}

	public void cellTransfer(GridCell from, short destinationSide) {
		final short x_mod;
		final short y_mod;
		switch (destinationSide) {
		case GridCell.LEFT:
			x_mod = -1;
			y_mod = 0;
			break;
		case GridCell.RIGHT:
			x_mod = 1;
			y_mod = 0;
			break;
		case GridCell.DOWN:
			x_mod = 0;
			y_mod = -1;
			break;
		case GridCell.UP:
			x_mod = 0;
			y_mod = 1;
			break;
		default:
			x_mod = 1;
			y_mod = 0;

		}
		final short act_x = (short) (from.x + x_mod);
		final short act_y = (short) (from.y + y_mod);
		if (act_x < 0 || act_x >= grid_w || act_y < 0 || act_y >= grid_h) {
			final short perimeterCoord;
			if (act_x < 0 || act_x >= grid_w) {
				perimeterCoord = getPerimeterFromBorderAndCoord(destinationSide, act_y);
			} else {
				perimeterCoord = getPerimeterFromBorderAndCoord(destinationSide, act_x);
			}
			if (organs.containsKey(perimeterCoord)) {
				if (organs.get(perimeterCoord) == ORGAN_HEART_IN) {
					// TODO for a while this should be just like this. if blood
					// arrives to heart, it's perfectly fine
				} else {
					++cells_lost;
				}
			} else {
				++cells_lost;
			}

		} else {
			for (int i = 0; i < contents.size; ++i) {
				final GridCell cell = contents.get(i);
				if (act_x == cell.x && act_y == cell.y) {
					final short inputSide = (short) ((destinationSide * 4) % 15);
					if (!cell.inputBloodCell(inputSide)) {
						++cells_lost;
					}
					break;
				}
			}
		}

	}

	private short getPerimeterFromBorderAndCoord(short destinationSide, short respectiveCoord) {
		switch (destinationSide) {
		case GridCell.UP:
			return (short) (this.grid_w + respectiveCoord + 2 * grid_h);
		case GridCell.DOWN:
			return (short) (respectiveCoord + grid_h);
		case GridCell.LEFT:
			return (short) respectiveCoord;
		case GridCell.RIGHT:
			return (short) (this.grid_w + respectiveCoord + grid_h);
		}
		return respectiveCoord;
	}

	public byte getGameState() {
		return this.game_state;
	}

	public void dispose() {
		blueArrowTexture.dispose();
		redArrowTexture.dispose();
		heartTexture.dispose();
		default_font.dispose();

	}
}
