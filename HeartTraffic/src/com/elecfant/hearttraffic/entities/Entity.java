package com.elecfant.hearttraffic.entities;

public class Entity {
	protected int x;
	protected int y;

	public void setPosition(final int x, final int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
}
