package com.elecfant.hearttraffic.entities;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class GridCell extends Entity {
	public final static short LEFT = 1;
	public final static short RIGHT = 4;
	public final static short UP = 2;
	public final static short DOWN = 8;
	public final static short HEART_IN = 1;
	public final static short HEART_OUT = 2;
	public final static short LUNGS_IN = 3;
	public final static short LUNGS_OUT = 4;
	public final static short ORDINARY = 0;
	public short in;
	public float rotation = 0;
	public short type;
	public TextureRegion textureRegion;
	public Grid parent;

	public Array<Float> bloodCells;
	public Array<Short> cellEntryPoints;
	private boolean direction_switch = true;
	private Array<Boolean> cellDirection;
	
	

	public GridCell(short type, short in, Grid parent) {
		this.type = type;
		this.in = in;
		this.bloodCells = new Array<Float>(true, 10);
		this.cellEntryPoints = new Array<Short>(true, 10);
		this.cellDirection = new Array<Boolean>(true, 10);
		this.parent = parent;
	}

	public short getDiscreetRotation() {
		this.rotation = (this.rotation + 360) % 360;
		return (short) Math.round(rotation / 90);
	}

	public short getRealInputs() {
		final short dr = getDiscreetRotation();
		byte real_in = (byte) in;
		real_in = (byte) (real_in >>> dr);
		real_in = (byte) (real_in | (in << 4 - dr));
		real_in = (byte) (real_in & 15);
		return real_in;
	}

	public boolean inputBloodCell(short side) {
		final short real_in = getRealInputs();
		if ((side & real_in) == side) {
			this.bloodCells.add(0f);
			this.cellEntryPoints.add(side);
			this.cellDirection.add(direction_switch);
			direction_switch ^= true;
			return true;
		}
		return false;
	}

	public Vector2 getCellPosition(int id, int w, int h) {
		float t = bloodCells.get(id).floatValue();
		short entry = cellEntryPoints.get(id).shortValue();
		Vector2 start;
		Vector2 mid = new Vector2(w / 2, h / 2);
		Vector2 end = new Vector2();
		start = getPointFromSide(entry, w, h);
		// checking if there is path straight forward
		short dest = this.getDestination(entry, cellDirection.get(id).booleanValue());
		end = getPointFromSide(dest, w, h);
		final Vector2 result;
		if (t < 0.5) {
			t *= 2;
			result = new Vector2(
					(float) (start.x * (1 - t) + mid.x * t),
					(float) (start.y * (1 - t) + mid.y * t)
					);
		} else {
			t = t * 2 - 1;
			result = new Vector2(
					(float) (mid.x * (1 - t) + end.x * t),
					(float) (mid.y * (1 - t) + end.y * t)
					);
		}
		return result;

	}

	private short getDestination(short entry, boolean celldirection) {
		short real_in = this.getRealInputs();
		short path_fwd = (short) ((entry * 4) % 15);
		short dest;
		if ((path_fwd & real_in) == path_fwd) {
			return path_fwd;
		} else {
			dest = (short) (real_in ^ entry);
			if (dest == 1 || dest == 2 || dest == 4 || dest == 8) {
				return dest;
			} else {

				short first = 0;
				short second = 0;
				for (short s : new short[] { 1, 2, 4, 8 }) {
					if ((dest & s) == s) {
						first = s;
						second = (short) (dest - s);
						break;
					}
				}
				if (celldirection) {
					return first;
				} else {
					return second;
				}
			}
		}
	}

	private Vector2 getPointFromSide(short entry, int w, int h) {
		switch (entry) {
		case GridCell.LEFT:
			return new Vector2(0, h / 2);
		case GridCell.UP:
			return new Vector2(w / 2, h);
		case GridCell.DOWN:
			return new Vector2(w / 2, 0);
		case GridCell.RIGHT:
			return new Vector2(w, h / 2);
		default:
			return new Vector2();
		}
	}

	public float getCellRotation(int id) {
		// TODO make some normal rotation
		return 0;
	}

	public void update(float timeElapsed) {
		for (int i = 0; i < bloodCells.size; ++i) {
			float f = bloodCells.get(i).floatValue();
			f += timeElapsed / 3;
			if (f >= 1) {
				parent.cellTransfer(this, getDestination(cellEntryPoints.get(i).shortValue(), cellDirection.get(i).booleanValue()));
				bloodCells.removeIndex(i);
				cellEntryPoints.removeIndex(i);
				cellDirection.removeIndex(i);
			} else {
				bloodCells.set(i, f);
			}
		}

	}

	public int killAllCells() {
		final int killed = bloodCells.size;
		this.bloodCells.clear();
		this.cellEntryPoints.clear();
		this.cellDirection.clear();
		return killed;

	}
}
