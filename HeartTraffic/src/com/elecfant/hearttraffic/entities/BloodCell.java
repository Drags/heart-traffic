package com.elecfant.hearttraffic.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class BloodCell extends Entity {
	private TextureRegion textureRegion;
	private Vector2 nextCheckpoint;
	private int speed = 30;

	public BloodCell(TextureRegion textureRegion, int x, int y) {
		this.x = x;
		this.y = y;
		this.textureRegion = textureRegion;
	}

	public void render(SpriteBatch batch) {
		batch.draw(textureRegion, this.x, this.y, textureRegion.getRegionWidth()/4, textureRegion.getRegionHeight()/4);
	}

	public void update(float timeElapsed) {
		final Vector2 dir = new Vector2(nextCheckpoint).sub(this.x, this.y);
		dir.nor();
		this.x = (int) (dir.x * speed * timeElapsed);
		this.y = (int) (dir.y * speed * timeElapsed);
	}

	public void setNextCheckpoint(Vector2 checkpoint) {
		this.nextCheckpoint = checkpoint;
	}
}
