package com.elecfant.hearttraffic;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "HeartTraffic";
		cfg.useGL20 = true;
		cfg.vSyncEnabled = true;
		cfg.width = 640;
		cfg.height = 480;
		
		new LwjglApplication(new HeartTrafficGame(), cfg);
	}
}
